﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo._05___Polymorphisme
{
    public class Animal
    {
        public string Name { get; set; }
        public string Race { get; set; }
        public virtual void Crier()
        {
            Console.WriteLine("Grrr");
        }
    }

    public class Chien : Animal
    {
        public string JouetFav { get; set; }
        public override void Crier()
        {
            Console.WriteLine("Wouf");
        }

        public void FaireLeBeau()
        {
            Console.WriteLine("Se lève et tend la patte");
        }

    }

    public class Chat : Animal
    {
        public string CachetteFav { get; set; }
        public override void Crier()
        {
            Console.WriteLine("Miaou");
        }

        public void Ronronner()
        {
            Console.WriteLine("Ron ron ron");
        }
    }
}
