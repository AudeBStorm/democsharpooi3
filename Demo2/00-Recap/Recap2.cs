﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace Demo2._00_Recap
//{
//    public class Recap2
//    {
//        //Polymorphisme
//        //Toute classe enfant est aussi du type parent, un Chien est un Animal
//        //Un chat est un animal
//        Animal Medor = new Chien(); //Casting implicite
//        Animal Garfield = new Chat(); //Casting implicite
//        ((Chien) Medor).Aboyer(); //Casting explicite, on devra recaster notre Medor qui est un Animal en Chien pour avoir accès aux méthodes et propriétés de Chien

//        //Classe Abstraite
//        abstract class ClasseParent
//        {
//            public string Prop1 { get; set; }
//            public abstract string Prop2 { get; set; }

//            public void Methode1()
//            {

//            }

//            public abstract void Methode2();
//        }

//        class ClasseEnfant : ClasseParent
//        {
//            public override string Prop2 { set; get; }
//            public override void Methode2()
//            {
//                Console.WriteLine("Methode2");
//            }
//        }

//        ClasseParent CA = new ClasseParent(); //La classe parent est abstract donc on ne peut pas créer de nouvelle instance de ClasseParent
//        ClasseEnfant CE = new ClasseEnfant();
//        ClasseParent CP = new ClasseEnfant();

//        //Classe Statique (et Propriété/Méthodes statiques)
//        public class ClasseStatique
//        {
//            public string Prop1 { get; set; }
//            static string Prop2 { get { return "Propriété 2"; } }

//            static double Addition(double nombre1, double nombre2)
//            {
//                return nombre1 + nombre2;
//            }
//        }
//        ClasseStatique CS = new ClasseStatique();
//        CS.Prop1 = "Propriété 1";
//        //Quand une méthode ou propriété est statiques,
//        //on peut l'appeler directement depuis le nom de la classe sans créer de nouvelle instance de cette classe
//        Console.WriteLine(ClasseStatique.Prop2);
//        ClasseStatique.Addition(12.5, 50.3);

//        //Interfaces
//        //C'est un "contrat" passé entre l'interface et la classe qui l'implémente
//        interface IPerson
//        {
//            string Nom { get; set; }
//            public string Prenom { get; set; }

//            public void DireBonjour();
//        }

//        interface IUser : IPerson {
//            string Login { get; set; }
//            string PassWord { set; }

//            void LirePost();
//        }

//        interface IAdmin : IUser
//        {
//            void EcrirePost();
//        }

//        class Personne : IUser, IAdmin
//        {
//            public string Login { get; set; }
//            public string PassWord { get; set; }
//            public string Nom { get; set; }
//            public string Prenom { get; set; }

//            public void DireBonjour()
//            {
//                Console.WriteLine("Bonjour");
//            }

//            public void DireAuRevoir()
//            {
//                Console.WriteLine("Au revoir");
//            }

//            public void LirePost()
//            {
//                Console.WriteLine("Je lis le super post posté sur JVC et je pleure");
//            }

//            public void EcrirePost()
//            {
//                Console.WriteLine("Jékri 1 post sur JVC lol");
//            }
//        }

//        IUser Alphonse = new Personne();
//        Alphonse.Nom;
//        Alphonse.DireBonjour();
//        Alphonse.LirePost();
//        Alphonse.EcrirePost(); //Impossible puisque Alphonse n'est pas un IAdmin

//        IAdmin Kevin = new Personne();
//        Kevin.Nom;
//        Kevin.DireBonjour();
//        Kevin.LirePost();
//        Kevin.EcrirePost(); //Possible puisque Kevin est un IUser


//        //Constructeurs
//        class PersonneBis
//        {
//            public string Nom { get; set; }
//            public string Prenom { get; set; }
//            public DateTime DateDeNaissance { get; set; }

//            public string Conjoint_e { get; set; }
//            public string Metier { get; set; }

//            public PersonneBis()
//            {

//            }
//            public PersonneBis(string Nom, string Prenom)
//            {
//                this.Nom = Nom;
//                this.Prenom = Prenom;
//            }
//            public PersonneBis(string Nom, string Prenom, DateTime DateNaiss) : this(Nom, Prenom)
//            {
//                DateDeNaissance = DateNaiss;
//            }

//        }

//        PersonneBis Person = new PersonneBis();
//        Person.Nom = "Beurive";
//        //Appelle le constructeur vide
//        PersonneBis Person = new PersonneBis();
//        //Appelle le constructeur avec deux params de type string
//        PersonneBis Person = new PersonneBis("Beurive", "Aude");
//        //Appelle le constructeur avec trois params, deux premiers string, troisième date
//        PersonneBis Person = new PersonneBis("Beurive", "Aude", new DateTime(1989, 10, 16));

//        //Dans le cadre de l'héritage,
//        //il faudra utiliser le mot clef "base" pour appeler le constructeur de la classe parent
//        class Animal
//        {
//            public string Nom { get; set; }
//            public string Race { get; set; }

//            public Animal(string Nom, string Race)
//            {
//                this.Nom = Nom;
//                this.Race = Race;
//            }
//        }

//        class Chien : Animal
//        {
//            public string JouetFav { get; set; }

//            public Chien(string Nom, string Race, string JouetFav) : base(Nom, Race)
//            {
//                this.JouetFav = JouetFav;
//            }

//            ~Chien()
//            {

//            }
//        }

//        using(PersonneBis Personne = new PersonneBis())
//        {
//            //Traitement à faire sur l'instance de l'objet crée dans le using
//        }
//        //Personne n'existe plus


//    }
//}
