﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Recap
{
    public class Recap1
    {
        /////  Les classes  /////
        // Représentent le modèle, la "maquette" à suivre d'un objet
        // Le mot clef class pour créer une classe

        //On peut définir le niveau d'accessibilité d'une classe mais aussi de ses... propriétés
        // public, private, protected, internal, protected internal, private protected
        public class Personne
        {
            /// Les propriétés ///
            // Ce sont les caractéristiques de notre objet 
            //  Les accesseurs get / set 
            //Les getters setters, nous permettent de passer une propriété
            //en lecture seule et/ou en écriture seule
            //voire même de la laisser exister uniquement dans la classe.
            //niveau d'accessibilité type nomProp { niveau d'accessibilité getter ; niveau d'accessibilité setter }
            public string LastName { get; set; }
            //Pour mettre une propriété en écriture seule :
            public string PassWord { private get; set; }
            //Pour mettre une propriété en lecture seule :
            public DateTime BirthDate { get; private set; }
            public string Nis { get; set; }

            //On peut également créer, pour chaque propriété, une varable privée et une publique avec ses accesseurs
            private string _FirstName;
            //Dans ce cas, on peut personnaliser notre getter et notre setter avec n'importe quel code, condition etc
            public string FirstName
            {
                get { return _FirstName.ToUpper(); }
                set { if (value.Length > 0) _FirstName = value; }
            }

            //Les méthodes
            //Pareil, on leur définit un niveau d'accessibilité afin de les rendre disponible pour toute la classe, tout le programme, etc

            //Pour déclarer une méthode :
            //niveau d'accessibilité, type de retour (void ou type), nom de la méthode, ( Les potentiels paramètres)
            //Sans retour de valeur = procédure
            public void DireBonjour()
            {
                Console.WriteLine("Bonjour");
            }
            //Avec retour de valeur 
            public int AvoirAge()
            {
                return DateTime.Now.Year - BirthDate.Year;
            }

            /////  Indexeur  /////
            //On aura besoin d'un dictionnaire
            Dictionary<string, Personne> Famille = new Dictionary<string, Personne>();

            //Pour accéder à la valeur à la clef X directement sur notre objet :
            //Grâce à cette méthode, on peut directement écrire 
            //NomVariablePersonne[Key];
            //pour obtenir le membre de la famille correspondant à la clef fournie
            public Personne this[string X]
            {
                get
                {
                    Personne p;
                    Famille.TryGetValue(X, out p);
                    return p;
                }
            }
            //Il nous faudra toujours une méthode pour ajouter de nouveaux éléments à notre dictionnaire
            public void AjouterMembre(Personne Membre)
            {
                Famille.Add(Membre.Nis, Membre);
            }

            //Surchage d'opérateur
            //Redéfinition du comportement des opérateurs +,-,*,/,%, ==, !=, !, >, <, >=, <=, |, &
            public static string operator +(Personne A, Personne B)
            {
                return $"{A.FirstName} et {B.FirstName} ont eu un enfant";
            }

            public static string operator +(Personne A, string B)
            {
                return "Pouet";
            }

            public static string operator -(Personne A, Personne B)
            {
                return $"{A.FirstName} et {B.FirstName} se sont séparés :'(";

            }

            ///// Héritage /////
            ///Quand on commence à avoir des similarités entre deux ou plus classes, il est peut-être temps de faire de l'héritage
            ///Par exemple un Chien et un Chat, ont tous les deux un nom, une race, une couleur etc, ils poussent tous les deux un cri
            ///Les deux, sont des animaux, nous pouvons donc faire hériter nos deux classes de Animal (qui aura, un nom, une race, une couleur, un cri...)
            //On mettra tous les propriétés et méthodes similaires dans la classe parent
            //Si on a une méthode similaire mais avec un comportement différent, on la déclarera en virtual
            //Et la méthode redéfinie en override
            class Animal
            {
                public string Name { get; set; }
                public virtual void Crier()
                {
                    Console.WriteLine("Grrrr");
                }
            }
            class Chien : Animal
            {
                public string JouetFav { get; set; }
                public override void Crier()
                {
                    Console.WriteLine("Wouf");
                }
            }
            class Chat : Animal
            {
                public override void Crier()
                {
                    Console.WriteLine("Miaou");
                }
            }

            class Ours : Animal
            {
                public int NombreSaumons { get ; set; }
            }
            
           
        }//Fin class Personne

    }
}
