﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo._04___Heritage
{
    public class Animal
    {
        public string Nom { get; set; }
        public string Race { get; set; }
        public string Couleur { get; set; }
        public int Age { get; set; }

        public string StylePoil { get; set; }

        public virtual void FaireBruit()
        {
            Console.WriteLine("Onk Onk");
        }

        public void VisiteVeto()
        {
            Console.WriteLine($"{Nom} est allé chez le vétérinaire le {DateTime.Now.ToString()}");
        }

        public virtual void JeSuis()
        {
            Console.Write("Je suis un animal");
        }
    }

    //public class Mammifere : Animal
    //{
    //    public string StylePoil { get; set; }
    //    public void Accoucher()
    //    {
    //        Console.WriteLine("Ouin ouin");
    //    }
    //}
    public class Chien : Animal
    {
        //public string Nom { get; set; }
        //public string Race { get; set; }
        public string JouetFav { get; set; }
        //public string Couleur { get; set; }
        //public int Age { get; set; }
        //public string StylePoil { get; set; }
        public string Metier { get; set; }
        

        public override void FaireBruit()
        {
            Console.WriteLine("Ouaf Ouaf");
        }

        //public void VisiteVeto()
        //{
        //    Console.WriteLine($"{Nom} est allé chez le vétérinaire le {DateTime.Now.ToString()}");
        //}
        public override void JeSuis()
        {
            base.JeSuis();
            Console.WriteLine(" et je suis un Chien");
        }
    }

    public sealed class Chat : Animal
    {
        //public string Nom { get; set; }
        //public string Race { get; set; }
        //public int Age { get; set; }
        //public string StylePoil { get; set; }
        //public string Couleur { get; set; }

        public string CachetteFavorite { get; set; }

        public override void FaireBruit()
        {
            Console.WriteLine("Mwraou");
            
        }

        public override void JeSuis()
        {
            base.JeSuis();
            Console.WriteLine(" et je suis un chat");
        }
    }


    //Avec le mot clef sealed devant la class Chat,
    //plus personne ne pourra faire de class qui hérite de Chat
    //public class ChatTransgenique : Chat
    //{

    //}
}
