﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo2._12___Events
{
    public class Chiot
    {
        private int _Appetit;
        public event FaimEventDelegate FaimEvent = null;
        public string Nom { get; set; }
        public int Appetit {
            get { return _Appetit; }
            private set
            {
                _Appetit = value;
                if (_Appetit == 0)
                {
                    FaimEvent?.Invoke();
                    //Si c'était une classe enfant qui devait appeler l'event, on appelerait la méthode protected DeclencheEvent();
                }

            }
        }

        public Chiot(string Name)
        {
            Nom = Name;
            Appetit = 10;
            FaimEvent += AlerteFaim;
            FaimEvent += Nourrir;
        }

        public void FaireSaVie()
        {
            Appetit--;
        }

        protected void DeclencheEvent()
        {

            FaimEvent?.Invoke();

        }

        private void AlerteFaim()
        {
            Console.WriteLine($"{Nom} a faim :'(");
        }

        private void Nourrir()
        {
            Appetit = 10;
        }

    }

    public class Avorton : Chiot
    {
        public Avorton(string Name) : base(Name)
        {
            DeclencheEvent();
        }
    }
    

}
