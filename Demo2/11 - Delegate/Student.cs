﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo2._11___Delegate
{
    public class Student
    {
        public delegate void DireBonjour(string Prenom);
        DireBonjour DB;
        public Student()
        {
            DB = null;
            //Exemple avec fonction anonyme 
            DB += delegate (string Prenom)
            {
                Console.WriteLine("Agouh Agouh " + Prenom);
            };
            
            //Exemple avec lambda
            //DB += (string Prenom) => { Console.WriteLine("Wesh alors " + Prenom); };
        }

        public void Bonjour(string Prenom)
        {
            if(DB != null)
            {
                DB.Invoke(Prenom);
                //DB(Prenom); //Same

            }
        }

        public void ApprendLeFrancais()
        {
            DB += EnFrancais;

        }
        public void ApprendLAnglais()
        {
            DB += EnAnglais;

        }
        public void ApprendLItalien()
        {
            DB += EnItalien;

        }
        public void ApprendLEspagnol()
        {
            DB += EnEspagnol;

        }
        public void ApprendLeRusse()
        {
            DB += EnRusse;

        }
        public void EnFrancais(string Prenom)
        {
            Console.WriteLine($"Bonjour {Prenom} :)");
        }

        public void EnAnglais(string Prenom)
        {
            Console.WriteLine($"Hello {Prenom} :)");
        }

        public void EnItalien(string Prenom)
        {
            Console.WriteLine($"Ciao {Prenom} :)");
        }

        public void EnEspagnol(string Prenom)
        {
            Console.WriteLine($"Hola {Prenom} :)");
        }

        public void EnRusse(string Prenom)
        {
            Console.WriteLine($"Privet {Prenom} :)");
        }
    }
}
