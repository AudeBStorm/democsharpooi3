﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo._03___Demo_Surchage_Operateur
{
    public class Panier
    {
        public List<Fruit> Fruits = new List<Fruit>();

        public static Panier operator +(Panier Pan1, Panier Pan2)
        {
            Panier PanierTotal = new Panier();
            //On ajoute à notre panier total, tous les éléments contenus dans le panier1
            foreach(Fruit Fruit in Pan1.Fruits)
            {
                PanierTotal.Ajouter(Fruit);
            }
            //On ajoute à notre panier total, tous les éléments contenus dans le panier2
            foreach(Fruit Fruit in Pan2.Fruits)
            {
                PanierTotal.Ajouter(Fruit);
            }
            return PanierTotal;
        }

        public void Ajouter(Fruit F)
        {
            Fruits.Add(F);
        }
    }


    public class Fruit
    {
        public string Name { get; set; }

    }
}
