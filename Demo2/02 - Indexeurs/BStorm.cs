﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Demo.Models;

namespace Demo.Indexeurs
{
    public class BStorm
    {
        Dictionary<string, Personne> Formateurs = new Dictionary<string, Personne>();

        public Personne this[string Id]
        {
            get {
                Personne p;
                Formateurs.TryGetValue(Id, out p);
                return p;
            }
        }

        public void AjouterFormateur(string Id, Personne formateur)
        {
            Formateurs.Add(Id, formateur);
        }

    }
}
