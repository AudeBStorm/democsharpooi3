﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo2._07_Classe_Statique
{
    static class Configurations
    {
        public static string ProductApi
        {
            get
            {
                return "https://masuperapi/products/";
            }
        }

        public static string CategoryApi
        {
            get
            {
                return "https://masuperapi/categories/";
            }
        }
    }
}
