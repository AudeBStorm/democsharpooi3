﻿using System;
using System.Collections.Generic;
using Demo.Models;
using Demo._03___Demo_Surchage_Operateur;
using Demo._04___Heritage;
using Poly = Demo._05___Polymorphisme;
using Demo.Indexeurs;
using Pers = Demo.Recap.Recap1.Personne;
using Anim = Demo.ClasseAbstraite;
using Demo2._07_Classe_Statique;
using InterAnim = Demo2._08___Interfaces;
using Construc = Demo2._09_Constructeur;
using Demo2._10___Exceptions;
using Demo2._11___Delegate;
using Demo2._12___Events;
using Demo2.Bonus_Nouveaute;

namespace Demo2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello, World!");

            #region Demo01 - Classes
            //Demo.Models.Example e = new Demo.Models.Example();
            Example e = new Example();
            Console.WriteLine(e.message);
            //On ne sait pas accéder à messagePrive, puisqu'elle a été déclarée comme privée
            //Console.WriteLine(e.messagePrive);
            e.message = "Nouveau Message";
            e.AfficheMessagePrive();

            //Pour instancier un nouvel objet de la class voulue :
            //List<Personne> personnes = new List<Personne>();

            //Personne maPersonne = new Personne();
            //maPersonne.Lastname = "Choucroute";

            //personnes.Add(maPersonne);

            //Personne maPersonne2 = new Personne();
            //maPersonne2.Lastname = "Beurive";
            //personnes.Add(maPersonne2);

            //On crée un nouvelle instance d'objet de type Personne
            Personne Aude = new Personne();

            //On assigne une valeur à son nom/prénom
            //Aude.LastName = "Beurive";
            Aude.FirstName = "Aude";

            //La modification de la date ne se fera pas puisqu'on a mis une condition dans le setter
            //La date de naissance doit être < à Date du Jour;
            Aude.BirthDate = new DateTime(2023, 10, 16);
            //Ici la modification de la date sera bien effectuée 
            Aude.BirthDate = new DateTime(1989, 10, 16);
            Console.WriteLine("Date de Naissance :");
            Console.WriteLine(Aude.BirthDate);

            //Affichage de la propriété qui nous renvoie la concaténation du nom/prénom
            Console.WriteLine(Aude.FullName);

            //Affichage d'une propriété en lecture seule :
            Console.WriteLine(Aude.NIS);
            //L'assignation d'une valeur sera impossible puisqu'en lecture seule
            //Aude.NIS = "14578965745";

            //Propriété en écriture seule
            Aude.PassWord = "1234";
            //La lecture de cette propriété est impossible puisqu'en écrture seule
            //Console.WriteLine(Aude.PassWord);
            #endregion

            #region Demo02 - Indexeurs
            ///////////////Demo Indexeurs ///////////////
            BStorm Bt = new BStorm();

            Personne formateur1 = new Personne()
            {
                FirstName = "Aude",
                LastName = "Beurive",
                BirthDate = new DateTime(1989, 10, 16)
            };
            Personne formateur2 = new Personne()
            {
                FirstName = "Samuel",
                LastName = "Legrain",
                BirthDate = new DateTime(1985, 09, 27)
            };

            Bt.AjouterFormateur("001", formateur1);
            Bt.AjouterFormateur("002", formateur2);

            Console.WriteLine($"Le.a formateurice à la clef 001 est : {Bt["001"].FullName}");
            Console.WriteLine($"Le.a formateurice à la clef 002 est : {Bt["002"].FullName}");
            //Bt["007"] nous renverra null puisque la clef n'est pas présente dans notre dictionnaire de formateurs
            Console.WriteLine($"Le.a formateurice à la clef 007 est : {Bt["007"] == null}");
            #endregion

            #region Demo03 - Surchage Opérateur
            Console.WriteLine();
            Console.WriteLine("Demo surchage");
            Panier PanierFruit1 = new Panier();
            PanierFruit1.Ajouter(new Fruit() { Name = "Banane" });
            PanierFruit1.Ajouter(new Fruit() { Name = "Pêche" });
            PanierFruit1.Ajouter(new Fruit() { Name = "Orange" });
            Console.WriteLine();
            Console.WriteLine("Contenu du panier 1 : ");
            foreach (Fruit F in PanierFruit1.Fruits)
            {
                Console.WriteLine(F.Name);
            }

            Panier PanierFruit2 = new Panier();
            PanierFruit2.Ajouter(new Fruit() { Name = "Ananas" });
            PanierFruit2.Ajouter(new Fruit() { Name = "Fraise" });
            Console.WriteLine();
            Console.WriteLine("Contenu du panier 2 : ");
            foreach (Fruit F in PanierFruit2.Fruits)
            {
                Console.WriteLine(F.Name);
            }
            Panier PanierFruit3 = new Panier();
            Console.WriteLine();
            Panier GrosPanier = PanierFruit1 + PanierFruit2 + PanierFruit3;
            Console.WriteLine("Contenu du gros panier : ");
            foreach (Fruit F in GrosPanier.Fruits)
            {
                Console.WriteLine(F.Name);
            }


            #endregion

            #region Demo04 - Héritage
            Animal NouvelleDecouverte = new Animal()
            {
                Nom = "Inconnu",
                Age = 50,
                Couleur = "Vert pomme",
                Race = "Inconnue",
                StylePoil = "Très très long"
            };
            NouvelleDecouverte.VisiteVeto();
            NouvelleDecouverte.FaireBruit();

            Chien Hector = new Chien()
            {
                Nom = "Hector",
                Couleur = "Marron et blanc",
                Race = "Corgi",
                JouetFav = "Un os en plastique qui fait pouic pouic",
                Age = 3,
                StylePoil = "Court",
                Metier = "Acteur de cinéma"
            };
            Hector.FaireBruit();
            Hector.VisiteVeto();
            Hector.JeSuis();

            Chat Garfield = new Chat()
            {
                Nom = "Garfield",
                Couleur = "Roux",
                Race = "Européen",
                Age = 8,
                StylePoil = "Long",
                CachetteFavorite = "Sous l'évier"
            };
            Garfield.FaireBruit();
            Garfield.VisiteVeto();
            Garfield.JeSuis();
            #endregion

            #region Demo05 - Recap1
            Pers AudeB = new Pers();
            AudeB.Nis = "1445894";
            AudeB.FirstName = "Aude";
            AudeB.LastName = "Beurive";

            Pers ThomasB = new Pers();
            ThomasB.Nis = "45879568";
            ThomasB.FirstName = "Thomas";
            ThomasB.LastName = "Beurive";

            AudeB.AjouterMembre(ThomasB);
            Console.WriteLine("Membre de la famille de " + AudeB.FirstName + " : " + AudeB["45879568"].FirstName);

            Pers Truc = new Pers();
            Truc.FirstName = "Truc";
            Pers Muche = new Pers();
            Muche.FirstName = "Muche";

            Console.WriteLine(Truc + Muche);
            #endregion

            #region Demo06 - Polymorphisme
            Poly.Chien Kevin = new Poly.Chien();
            Kevin.Name = "Kevin";
            Poly.Animal JeanKevin = new Poly.Chien();
            ((Poly.Chien)JeanKevin).FaireLeBeau();
            JeanKevin.Name = "Jean-Kevin";
            Object HenriKevin = new Poly.Chien();
            //HenriKevin.Name = "Bleb";
            ((Poly.Chien)HenriKevin).Name = "Henri Kevin";
            #endregion

            #region Demo07 - Classe Abstraite
            //Anim.Animal Animal = new Anim.Animal();
            Anim.Chien Chien = new Anim.Chien();
            Anim.Animal Serge = new Anim.Chien();
            Serge.Crier();
            #endregion

            #region Demo07bis - Classe statique
            Console.WriteLine(Math.PI);
            Console.WriteLine(DateTime.Now);
            Console.WriteLine(Configurations.ProductApi);
            #endregion

            #region Demo08 - Interfaces
            InterAnim.Chien Medor = new InterAnim.Chien();
            InterAnim.Vache Marguerite = new InterAnim.Vache();

            InterAnim.IHerbivore Marg2 = Marguerite;
            InterAnim.ICarnivore Med = Medor;

            InterAnim.Chat Felix = new InterAnim.Chat();
            ((InterAnim.IHerbivore)Felix).Brouter();
            #endregion

            #region Demo09 - Constructeurs
            Construc.Chien Rufus = new Construc.Chien();
            Console.WriteLine($"Nom de Rufus : {Rufus.Name}, Jouet pref de Rufus : {Rufus.JouetFav}, Date de naissance de Rufus : {Rufus.DateNaissance.ToShortDateString()}");
            Construc.Chien Rantanplan = new Construc.Chien("Rantanplan", "Os qui fait pouet");
            Console.WriteLine($"Nom de Rantanplan : {Rantanplan.Name}, Jouet pref de Rufus : {Rantanplan.JouetFav}, Date de naissance de Rufus : {Rantanplan.DateNaissance.ToShortDateString()}");
            Construc.Chien Nouille = new Construc.Chien("Nouille", "Une balle de tennis", new DateTime(2022, 01, 16));
            Console.WriteLine($"Nom de Nouille : {Nouille.Name}, Jouet pref de Rufus : {Nouille.JouetFav}, Date de naissance de Rufus : {Nouille.DateNaissance.ToShortDateString()}");

            Nouille.Dispose();
            #endregion

            #region Demo10 - Exceptions
            int Nombre1 = 15;
            int Nombre2 = -5;
            int NombreTotal = 0;
            DemoExcep DE = new DemoExcep();
            try
            {
                DE.AdditionEntrePositif(Nombre1, Nombre2);

            }
            catch (ArgumentException ex)
            {
                Console.WriteLine(ex.Message);
            }

            string Nom = "Ly";
            try
            {
                if(Nom.Length <= 2)
                {
                    throw new MaSuperException();
                }
                Nom.Substring(2, 2);
            }
            catch(MaSuperException ex)
            {
                Console.WriteLine(ex.Message);
            }

            try
            {
                NombreTotal = Nombre1 / Nombre2;

            }
            catch(DivideByZeroException ex)
            {
                Console.WriteLine("Vous avez tenté de divisé par 0");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.GetType());
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Console.WriteLine(NombreTotal);
            }


            
            //string Bonjour = null;
            //string BonjourSub = "";
            //try
            //{

            //    Bonjour.Substring(2);

            //}
            //catch (NullReferenceException ex)
            //{
            //    Console.WriteLine(ex.Message);
            //}
            #endregion

            #region Demo11 - Delegate
            Student Bobby = new Student();
            Bobby.ApprendLItalien();
            Bobby.ApprendLeRusse();
            Bobby.Bonjour("John");

            #endregion

            #region Demo 12 - Events
            Chiot Kevin2 = new Chiot("Kevin");
            int i = 0;
            while ( i <= 21)
            {
                Kevin2.FaireSaVie();
                Console.WriteLine(Kevin2.Appetit);
                i++;
            }
            #endregion

            List<int> MaList = new List<int>();
            MaList.Add(2);
            Console.WriteLine(  MaList[0] );//2


            #region Demo 13 - Generic Action & Func
            Action<string> DemoAction = new Action<string>(AfficherBonjour);
            Action<string, string> DemoAction2 = new Action<string, string>(AfficherBonjour2);
            DemoAction += AfficherBonjour;
            DemoAction.Invoke("Bienvenue et bonjour, c'est presque vendredi");
            DemoAction2.Invoke("Bonjour", "Mardi");
            #endregion

            void AfficherBonjour(string Message)
            {
                Console.WriteLine(Message);
            }
            void AfficherBonjour2(string Message, string Jour)
            {
                Console.WriteLine(Message + " nous sommes " + Jour);
            }

            Func<string> FuncSimple = new Func<string>(DireBonjour);
            string Resultat = FuncSimple.Invoke();
            Console.WriteLine(Resultat);
            Console.WriteLine(FuncSimple.Invoke());

            string DireBonjour()
            {
                return "Bonjour";
            }

            Func<int, int, int> Operation = new Func<int, int, int>(Addition);
            Console.WriteLine( Operation.Invoke(3, 2) );
            int Addition(int Nombre1, int Nombre2)
            {
                return Nombre1 + Nombre2;
            }

            //Nouveautés
            Film MonFilm = new Film();
            Console.WriteLine(MonFilm.Titre);
            MonFilm.Pub();
            Console.ReadLine();
        }
    }
}
