﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Models.SubModels
{
    public class OtherExample
    {
        //Comme la portée est descendante, on peut créer un objet du namespace au dessus
        //Sans avoir à faire de using
        Example ex = new Example();

        public OtherExample()
        {

        }
    }
}
