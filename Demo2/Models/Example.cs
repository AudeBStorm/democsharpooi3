﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Models
{
    public class Example
    {
        
        //On aura accès à message depuis n'importe quelle autre class
        public string message = "Bonjour";
        //On n'aura accès à messagePrive que dans cette classe ci
        private string _messagePrive = "Bonjour mais caché";
        public Example()
        {
            Console.WriteLine();
            //On devra faire un using pour utiliser un objet dans un sous namespace
            //OtherExample oe = new OtherExample();
        }

        public void AfficheMessagePrive()
        {
            Console.WriteLine(_messagePrive);
        }
    }

    namespace Choucroute
    {
        //Cas assez peu rencontré
    }
}
