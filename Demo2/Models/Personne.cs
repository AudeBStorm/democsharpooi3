﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Models
{
    //Le mot clef "class", définit que le type qu'on va construire ici sera une classe :
    public class Personne
    {
        //Petit raccourci : écrire prop puis appuyer deux sur tab

        //On déclare les propriétés privées :
        //private string _LastName;
        private string _FirstName;
        private DateTime _BirthDate;
        private string _NIS;
        private string _PassWord;

        //On déclare des propriétés pour l'accesseur/mutateur
        //Propriétés accessibles en lecture et écriture sans vérification
        //public string LastName
        //{
        //    get { return _LastName; }
        //    set { _LastName = value; }
        //}
        public string FirstName 
        { 
            get { return _FirstName; } 
            set { _FirstName = value; }
        } 
        //Proprété accesible en lecture et écriture avec vérification
        public DateTime BirthDate
        {
            get { return _BirthDate; }
            set
            {
                if (value < DateTime.Now)
                {
                    _BirthDate = value;
                }
            }
        }
        //Propriétés bonus 
        public string FullName
        {
            get { return _FirstName + " " + LastName; }
        }

        public string Presentation
        {
            get { return $"Bonjour, je m'appelle {FullName}, je suis née le {_BirthDate.ToString()}"; }
        }

        //Propriété en lecture seule (On peut la consulter mais jamais modifier sa valeur)
        //2 façons de le faire :
            //1) En ne mettant pas de set
        //public string NIS
        //{
        //    get { return _NIS; }
        //}
            //2) En mettant un set privé
        public string NIS
        {
            get { return _NIS; }
            private set { _NIS = value; }
        }

        //Propriété en écriture seule (on pourra modifier sa valeur, mais jamais lire son contenu)
        //Pareil, 2 façons
            //1)
        //public string PassWord
        //{
        //    set { _PassWord = value; }
        //}
            //2)
        public string PassWord
        {
            private get { return _PassWord; }
            set {_PassWord = value; }
        }

        //Autopriétés
        public string LastName { get; set; }


        public List<Hobby> Hobbies;
        public Personne()
        {

        }
    }

    public class Hobby
    {
        public string name;
        public int duree;
    }


}
