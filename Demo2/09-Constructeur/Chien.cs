﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo2._09_Constructeur
{
    public class Animal
    {
        public string  Name { get; set; }

        ~Animal()
        {
            Console.WriteLine("Animal collecté par les éboueurs C#");
        }
        public Animal()
        {

        }
        public Animal(string Name)
        {
            this.Name = Name;
        }

        public void Naissance()
        {
            Console.WriteLine("Youmpi je suis né ouin ouin");
        }

    }
    public class Chien : Animal, IDisposable
    {
        //Début démo constructeur, à compléter
        //public string Nom { get; set; }
        public string JouetFav { get; set; }

        public DateTime DateNaissance { get; set; }

        ~Chien()
        {
            Console.WriteLine("Chien a été supprimé");
        }
        public Chien()
        {
           
        }

        //Le mot clef base sert à accéder au parent
        public Chien(string Nom, string Jouet) : base(Nom)
        {
            JouetFav = Jouet;
            DateNaissance = DateTime.Now;
            base.Naissance();

        }

        //Le mot clef this sert à accéder à la classe courante
        public Chien(string Nom, string Jouet, DateTime DateNaissance) : this(Nom, Jouet)
        {
            this.DateNaissance = DateNaissance;
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }

    //Chien Kevin = new Chien()
    //Kevin.Nom = "Kevin";
    //

    //Chien Kevin = new Chien("Kevin", "L'Os qui fait Pouet")
}
