﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.ClasseAbstraite
{
    public abstract class Animal
    {
        public string Name { get; set; }
        public abstract void Crier();

    }

    public class Chien : Animal
    {
        public string JouetFav { get; set; }
        public override void Crier()
        {
            Console.WriteLine("Wouf");
        }
    }

    public class Chat : Animal
    {
        public string CachetteFav { get; set; }
        public override void Crier()
        {
            Console.WriteLine("Miaou");
        }
    }



}
