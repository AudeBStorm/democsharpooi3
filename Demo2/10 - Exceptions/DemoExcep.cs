﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo2._10___Exceptions
{
    public class DemoExcep
    {
        public double AdditionEntrePositif(int A, int B)
        {
            if( A <= 0)
            {
                throw new ArgumentException("Le chiffre A n'est pas positif");
            }
            if(B <= 0)
            {
                throw new ArgumentException("Le chiffre B n'est pas positif");
            }
            return A + B;
        }
    }

    public class MaSuperException : Exception
    {
        public MaSuperException() : base("Ce nom est trop petit pour être substring")
        {

        }
    }
}
