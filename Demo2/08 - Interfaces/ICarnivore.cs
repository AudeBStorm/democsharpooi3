﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo2._08___Interfaces
{
    public interface ICarnivore
    {
        int NbCanines { get; set; }
        void Devorer();
        
    }
}
