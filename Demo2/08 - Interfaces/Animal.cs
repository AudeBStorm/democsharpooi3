﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo2._08___Interfaces
{
    public abstract class Animal : IHerbivore, ICarnivore
    {
        public int NbCanines { get; set; }
        public int NbEstomacs { get; set; }
        public string Name { get; set; }
        public abstract void Crier();

        public void Devorer()
        {
            Console.WriteLine("Krunch Krunch");
        }

        public void Brouter()
        {
            Console.WriteLine("Chomp chomp");
        }


    }

    public class Chien : Animal
    {
        public string JouetFav { get; set; }
        public override void Crier()
        {
            Console.WriteLine("Wouf");
        }

    }

    public class Chat : Animal
    {
        public string CachetteFav { get; set; }
        public override void Crier()
        {
            Console.WriteLine("Miaou");
        }
    }

    public class Vache : Animal
    {
        public override void Crier()
        {
            Console.WriteLine("Moooooo");
        }

    }



}
