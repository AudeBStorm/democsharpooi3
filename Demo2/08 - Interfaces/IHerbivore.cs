﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo2._08___Interfaces
{
    public interface IHerbivore
    {
        int NbEstomacs { get; set; }
        void Brouter();
        
    }
}
