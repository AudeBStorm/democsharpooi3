﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace Demo2.Bonus_Nouveaute
{
    public class Film
    {
        public string Titre { get; } = "Le film : Le retour";
        public string Realisateurice { get; } = "Aude Beurive";

        //Lambda pour le getter
        //public string Description {  
        //    get
        //    {
        //        return $"{Titre} réalisé par {Realisateurice}";
        //    } 
        //}
        public string Description => $"{Titre} réalisé par {Realisateurice}";

        //Exemples de lambda pour les méthodes
        //public int Addition(int Nombre1, int Nombre2)
        //{
        //    return Nombre1 + Nombre2;
        //}

        //Expression lambda
        public int Addition(int Nombre1, int Nombre2) => Nombre1 + Nombre2;

        //public void Pub()
        //{
        //    Console.WriteLine("Un super film qu'il est bien");
        //}
        public void Pub() => WriteLine("Un super film qu'il est bien");
    }
}
